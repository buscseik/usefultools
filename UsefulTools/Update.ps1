﻿Add-Type -AssemblyName System.IO.Compression.FileSystem
function Unzip
{
    param([string]$zipfile, [string]$outpath)

    [System.IO.Compression.ZipFile]::ExtractToDirectory($zipfile, $outpath)
}


Function Implement-OwnCertificate
{
<#
   
.DESCRIPTION
   This function will implement certificate of kriszlab.ddns.net to Trusted Root Certificate Authorities


.EXAMPLE
   Implement-OwnCertificate
#>


    $var=Get-ChildItem -Path Cert:\LocalMachine\Root\ | Where-Object {$_.Subject -like "*kriszlab*"}

    if ($var.Issuer -eq "CN=kriszlab.ddns.net, OU=KriszLab, O=KriszLab, L=Dublin, S=Dublin, C=ie")
    {
        return $true
    }

    
    Write-host "The Update-UseFulTools commandLet will add self signed certificate of kriszlab.ddns.net to your system in order to complete update."
    
    do
    {
        $ContinueLoop=$true
        $Answer=Read-host "Do you allow script to add certificate to system?(Y/N)"
        If($Answer -eq "n" -or $Answer -eq  "y")
        {
            $ContinueLoop=$false
        }
    }while ($ContinueLoop)
    if($Answer -eq "n")
    {
        
        return $false
    }

$Cert=@"
-----BEGIN CERTIFICATE-----
MIIDXjCCAkYCCQDjkb4ATGQwUTANBgkqhkiG9w0BAQsFADBxMQswCQYDVQQGEwJp
ZTEPMA0GA1UECAwGRHVibGluMQ8wDQYDVQQHDAZEdWJsaW4xETAPBgNVBAoMCEty
aXN6TGFiMREwDwYDVQQLDAhLcmlzekxhYjEaMBgGA1UEAwwRa3Jpc3psYWIuZGRu
cy5uZXQwHhcNMTcwMTMwMjIxMDEwWhcNMTgwMTMwMjIxMDEwWjBxMQswCQYDVQQG
EwJpZTEPMA0GA1UECAwGRHVibGluMQ8wDQYDVQQHDAZEdWJsaW4xETAPBgNVBAoM
CEtyaXN6TGFiMREwDwYDVQQLDAhLcmlzekxhYjEaMBgGA1UEAwwRa3Jpc3psYWIu
ZGRucy5uZXQwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDhpKOfgL8F
WdU3Z+qYsKPaYFDyyu+rNITYDE1APUWF2yz1UG0euUffG8QLrKOWJYw8qtYTxeBB
h7fgsRtOkqkMnboYu2ZpxCqOP4zb4bYfjCwq7vaGFO2bIVi74nEhaCeHIrvpK1CB
eXd4cSbrTdSh3kauwaqSQomAhDrdxBEOJEzUw+tD3/MzcdU8RQ1hSUgHSbtRJ7RQ
yEdm5jUVvTQVXZa9GRTunJbgt6Ushq8HneuSOfgRJJkTkPuV/TdMYqd4Guaa7EAF
fOGqPNjcWezpVokvkxnuSqVpe9cG1JneYG2ZJm/4JJEzLE4pnV62hMeqk55wgeeY
gcTUt21ZwrO5AgMBAAEwDQYJKoZIhvcNAQELBQADggEBAN5cO2KDJC1WZqE8tmZK
F+pFY4zP4S36fDJaMmP4Pwyn89tXPFFnGceFxwOez3OIRoAexWFxlhGsap19q4gL
cQnrsDsdUnNP/PvWei+521Z0XP4Mqq00dgls6SpaI2VyMlq/qHFopRAF7RWEWVAv
xzlj6TkpaVB9m4qUdpo7I1pgTErqNc0o4nLxrgaOD5AwWpLIBYC5tjG6G2xQpkLZ
1SRnKaf2SWrh+4id8c4xg6ZMAABgrMLzVtrEpCMuBhsoZun3lf+/aCxZF2V5Y5ZF
ueR7xpclCf8HHBFVlvX0lwAJ5yhmtFzG5PCoYHdjEmp9SMtJ9ViZ13LZ1uhpUo8B
UTo=
-----END CERTIFICATE-----
"@    
$Cert | Set-Content .\1.crt

Import-Certificate .\1.crt -CertStoreLocation Cert:\LocalMachine\Root

rm .\1.crt

return $true
}

function Update-usefultools()
{
<#
   
.DESCRIPTION
   This function give a single command option to update the ATS_General module.

.EXAMPLE
   Update-usefultools

#>
    #Update -SourceLink https://kriszlab.ddns.net:999/buscseik/ATS_General/repository/archive.zip?ref=master -moduleName "ATS_General"
	Update -SourceLink https://bitbucket.org/buscseik/usefultools/get/master.zip -moduleName "usefultools"





	
}


function Update
{

    param($SourceLink, $moduleName)

    $originalLocation=Get-Location
    
    Set-Location $env:temp
    (mkdir $moduleName ) >$null 2>&1
    cd $moduleName
    rm *  -force -Recurse
    $AllowUpdate=$true



    #if(-not $(Implement-OwnCertificate))
    #{
    #    Write-Host "Certificate required to complete update."
    #}



    $errorMessage=@"

An error occured during update.
Please check followings:
    -Internet connection
    -Make sure no other instance of modul has been loaded (best practice just to close rest of the powershell consoles)
    -Make sure GitLab server available (https://kriszlab.ddns.net:999/)

You can download ans install manually as an automatic option from $SourceLink.

"@
    try
    {
        Invoke-WebRequest -Uri "$SourceLink" -OutFile "module.zip"
    }
    catch
    {
        
        $AllowUpdate=$false
    }

    if($AllowUpdate -eq $true)
    {
        try
        {
            unzip -zipFile "$(get-location)\module.zip" -outpath "$(get-location)\"
            $FolderName = Get-ChildItem | where Name -Like "*$moduleName*"
            cd $FolderName
        }
        catch
        {
            "Somethin went wrong. Please try to download an update module manually `nYou can download most up to date package from following link `n$SourceLink  `n`n Need to overwrite the following folder: `n$($env:PSModulePath.Split(";")[0]+"\$($moduleName)")"
            $AllowUpdate=$false
        }
    }

    if($AllowUpdate -eq $true)
    {
        $Target=$env:PSModulePath.Split(";")[0]
        rm "$Target\$moduleName" -Force -Recurse
        #rm "$Target\UseFulTools\UsefullTool-Module.psm1"
        #rm "$Target\UseFulTools\UseFulTools.ps1"
        #rm "$Target\UseFulTools\UseFulTools.psd1"
        mv ".\$moduleName" "$Target\$moduleName" -Force 
        #mv .\UseFulTools\UsefullTool-Module.psm1 "$Target\UseFulTools\UsefullTool-Module.psm1" -Force 
        #mv .\UseFulTools\UseFulTools.ps1 "$Target\UseFulTools\UseFulTools.ps1" -Force 
        #mv .\UseFulTools\UseFulTools.psd1 "$Target\UseFulTools\UseFulTools.psd1" -Force 
        cd $env:temp
        rm "$($env:temp)\$moduleName" -Force -Recurse
        Write-Host "Newest version successfully downloaded and updated."
    }
    else
    {
        Write-Host $errorMessage
    }
     
    Set-Location $originalLocation
}
